import AvatarSrcPath from '../assets/img/Design.svg';
import BikeWheelSrcPath from '../assets/img/bike_wheel.svg';
import LaptopSrcPath from '../assets/img/laptop.svg';
import BooksSrcPath from '../assets/img/books.svg';
import KeyboardSrcPath from '../assets/img/keyboard.svg';
import PansSrcPath from '../assets/img/pans.svg';
import KeyboardFrontSrcPath from '../assets/img/keyboard_front.svg';
import LaptopFrontSrcPath from '../assets/img/laptop_front.svg';

export {
    AvatarSrcPath,
    BikeWheelSrcPath,
    LaptopSrcPath,
    BooksSrcPath,
    KeyboardSrcPath,
    PansSrcPath,
    KeyboardFrontSrcPath,
    LaptopFrontSrcPath,
};