import './styles/globals.module/index.css';
import './styles/main_section.module/index.css';
import './styles/avatar.module/index.css';
import './styles/image_interests.module/index.css';

import {
  AvatarSrcPath,
  BikeWheelSrcPath,
  LaptopSrcPath,
  BooksSrcPath,
  KeyboardSrcPath,
  PansSrcPath,
  KeyboardFrontSrcPath,
  LaptopFrontSrcPath,
} from './modules/svgs';

function htmlImageBuilder(id, src) {
  const image_div = document.getElementById(id);
  const image_html_img = document.createElement('object');
  image_html_img.data = src;
  image_html_img.addEventListener('load', function (_e) {
    const gElement = this.contentDocument.querySelector('g');
    gElement.addEventListener('mouseover', () => {
      image_html_img.classList.add("hovering");
    });
    gElement.addEventListener('mouseout', () => {
      image_html_img.classList.remove("hovering");
    });
    gElement.addEventListener('click', () => {
      console.log('Hello', id)
      window.location.href = `/${id}.html`
    })
  })
  image_div.appendChild(image_html_img);
};

function component() {
  // This is to bring the SVG directly, without using an HTML Image element. 

  //Start avatar image creation
  htmlImageBuilder('avatar_div', AvatarSrcPath);
  //End avatar image creation

  //Start wheel image creation
  htmlImageBuilder('bike_wheel', BikeWheelSrcPath);
  //End wheel image creation

  //Start laptop image creation
  htmlImageBuilder('laptop', LaptopSrcPath);
  //End laptop image creation

  //Start books image creation
  htmlImageBuilder('books', BooksSrcPath);
  //End books image creation

  //Start keyboard image creation
  htmlImageBuilder('keyboard', KeyboardSrcPath);
  //End keyboard image creation

  //Start pans image creation
  htmlImageBuilder('pans', PansSrcPath);
  //End pans image creation


  //Start keyboard front image creation
  htmlImageBuilder('keyboard_front', KeyboardFrontSrcPath);
  //End keyboard front image creation

  //Start laptop front image creation
  htmlImageBuilder('laptop_front', LaptopFrontSrcPath);
  //End laptop front image creation

  //Creating the drop down abilities for the mobile menu
  const dropDown = document.getElementById('drop_down_circle')
  dropDown.addEventListener('click', (_e) => {
    console.log('Dropping down');
    const mobileMenu = document.getElementById("menuSelect");
    mobileMenu.classList.toggle("onAndOffSwitch");
    // mobileMenu.classList.toggle("buttons_div");
  }
  )
  //End creation

};

component();