const InterpolateHtmlPlugin = require("interpolate-html-plugin");
const NodePolyfillPlugin = require("node-polyfill-webpack-plugin");
const HtmlWebPackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'output.bundle.js',
  },
  devServer: {
    hot: false,
  },
  plugins: [
    new InterpolateHtmlPlugin({
      'NODE_ENV': 'development'
    }),
    new NodePolyfillPlugin(),
    new HtmlWebPackPlugin({
      title: 'Learning App For Johnny Pezzimenti',
      template: './src/index.html',
    }),
    new HtmlWebPackPlugin({
      title: 'Bike Blog',
      template: './src/bike_wheel.html',
      filename: 'bike_wheel.html',
    }),

    new HtmlWebPackPlugin({
      title: 'Programming',
      template: './src/laptop.html',
      filename: 'laptop.html',
    }),

    new HtmlWebPackPlugin({
      title: 'Teaching',
      template: './src/books.html',
      filename: 'books.html',
    }),

    new HtmlWebPackPlugin({
      title: 'Music',
      template: './src/keyboard.html',
      filename: 'keyboard.html',
    }),

    new HtmlWebPackPlugin({
      title: 'Cooking',
      template: './src/pans.html',
      filename: 'pans.html',
    })],
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
    ],
  },
};
