# vanilla-js-starter-kit
This is a starter kit for building Vanilla JS apps with learning purposes. It has configurations set up for Webpack and linter.

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

- [npm](https://medium.com/r/?url=https%3A%2F%2Fwww.npmjs.com).
- [Webpack](https://webpack.js.org/).

### Setup
- Change directory into the cloned folder with `cd vanilla-js-starter-kit`. You may choose to change the folder name to match your intended project.
- Run `npm install` in your terminal/command line to install dependencies.
- Run `npm start` in your terminal/command line to start developing your app.
- Run `npm run production` when you are done and ready to deploy to production.

## Acknowledgments

- [Linter configuration files](https://medium.com/r/?url=https%3A%2F%2Fgithub.com%2Fmicroverseinc%2Flinters-config%2Ftree%2Fmaster%2Fjavascript) were borrowed from [Microverse Inc.](https://www.microverse.org)
